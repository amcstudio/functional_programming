const R = require('ramda');
const { prop,compose, add } = R;
const { log } = console;



const user1 = {name:'Michel',email:'m@m.com'};
const sayHello = (toWhom) => `Hello ${ toWhom }!`;

const getName = (user) => user.name;

const greeting = (user) => {
    return  sayHello(getName(user));

};

const greets = compose(sayHello,getName)(user1);

console.log(greeting(user1));

console.log(compose(sayHello, getName)(user1));

console.log(greets);
console.log('>>>>>>>>>>>>>>>> curried function 1');
//  curried function 2



const tweentyThree = add(10,13);
const addTen = add(10);
const twoThree = addTen(13)

console.log(tweentyThree);
console.log(twoThree);
console.log(tweentyThree === twoThree);
console.log('>>>>>>>>>>>>>>>> curried function 2');


const greetInOneLine  = compose(R.concat('Hello '),prop('name'));
console.log(greetInOneLine({name : 'world!!!'}));
console.log('>>>>>>>>>>>>>>>> log function 3');