const { log } = console;
const R = require('ramda');
const {compose, concat} = R;


function HayStack(needle){
    this.needle = R.toUpper(needle);
}

// const haystack = (needle) => new HayStack(needle); same as 
// annotation haystack :: String -> HayStack
const haystack = R.construct(HayStack);

// const getNeedle = (hs) => hs.needle; same as
// annotation getNeedle :: HayStack -> String 
const getNeedle = R.prop('needle');

// annotation= concatTo String -> string
const concatTo = R.flip(concat);
// const whatWeFound = (what) => `Hay hey we have found ${ what } in a haystack!`;
// annotation = whatWeFound - String -> String 
const whatWeFound = compose(concat('Hay hey we have found'),concatTo('in a haystack'));
// annotation=  map :: Functor f => (a -> Boolean) -> f a -> f b
const map = R.map;
//annotation=  HayStack -> String
const needleFromHaystack = compose(whatWeFound,getNeedle);

// tediousWork :: String -> String
const tediousWork = compose(needleFromHaystack,haystack);

//annotation :: [String] -> String
const intoHayStacks = map(haystack);

log(
    map(haystack)(['diamond','old coin','my wallet','hay'])
);
// filter :: Filterable f => (a -> Boolean) -> f a -> f a
log(
    R.filter(R.lt(50),{a:60,b:70,c:7,d:80})
);

log('update on git')
