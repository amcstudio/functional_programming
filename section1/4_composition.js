const { log } = console;
const R = require('ramda');
const {map ,compose, equals} = R;

function Num(a) {
    this.value = a;
}

Num.prototype.equals = function(b) {
    return this.value === b.value;
}
Num.prototype.map = function(fn) {
    return new Num(fn(this.value));
}
const num = R.construct(Num);

// annotatio :: addition -> Number -> Number
const addition = R.curry((x,y) => x + y);
addition.identity = 0;


// annotatio :: multiplication -> Number -> Number
const mult = R.curry((x,y) => x * y);
mult.identity = 1;

// annotatio :: division -> Number -> Number
const division = R.curry((n,d) => n / d);
division.rightIdentity = 1;


//associativity  (a * b ) * c = a * (b * c);
const sum1 = R.compose(addition(10), addition(20),addition(40),addition(50));
const sum2 = R.compose(
    addition(10),
 R.compose(
     addition(20),addition(40)
     ),addition(50));

const data = [1,4,6,12];
const output = data.map(addition(30)).map(addition(40))
.map(addition(10));

log('output is'+output);

const op1 = compose(map(addition(10)),map(mult(40)),map(addition(30)));
const op2 = compose(addition(10),mult(40),addition(30));

const output1 = map(op2, data);
log(output1);


const output2 = map(op2, num(20));
log(output2);